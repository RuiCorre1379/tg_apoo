/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg4emlinha;

/**
 *
 * @author Pc
 */
public class Pessoa {
    
    String nome;
    /**
     * nome do jogador
     */
    int njogador;
    /**
     * numero particular de cada jogador
     */
    int idade;
    /**
     * idade de cada jogador
     */
    int jogosg;
    /**
     * jogos livres ganhos de cada jogador
     */
    int campg;

    /**
     * campeonatos ganhos pelo jogador
     */
    /**
     * construtor com 5 parametros(nome,numero do jogador,idade,numero de jogos
     * ganhos e nº de campeonatos ganhos
     *
     * @param nome
     * @param njogador
     * @param idade
     * @param jogosg
     * @param campg
     */
    public Pessoa(String nome, int njogador, int idade, int jogosg, int campg) {

        this.nome = nome;
        this.njogador = njogador;
        this.idade = idade;
        this.jogosg = jogosg;
        this.campg = campg;
    }

    public Pessoa() {
        /**
         * construtor vazio
         */
    }

    /**
     * define o nome do jogador
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * vai buscar o nome do hogador
     *
     * @return nome do jogador
     */
    public String getNome() {
        return nome;
    }

    /**
     * define o nº particular do jogador
     *
     * @param njogador
     */
    public void setNjogador(int njogador) {
        this.njogador = njogador;
    }

    /**
     * vai buscar o nº particular do jogador
     *
     * @return o nº numero particular do jogador
     */
    public int getNjogador() {
        return njogador;
    }

    /**
     * define a idade do jogador
     *
     * @param idade
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * vai buscar a idade do jogador
     *
     * @return idade do jogador
     */
    public int getIdade() {
        return idade;
    }

    /**
     * define o numero de jogos ganhos pelo jogador
     *
     * @param jogosg
     */
    public void setNumerojganhos(int jogosg) {
        this.jogosg = jogosg;
    }

    /**
     * vai buscar o numero de jogos ganhos pelo jogador
     *
     * @return nº de jogos ganhos
     */
    public int getJogosg() {
        return jogosg;
    }

    /**
     * define de campeonatos ganhos pelo jogador
     *
     * @param campg
     */
    public void setNcampg(int campg) {
        this.campg = campg;
    }

    /**
     * vai buscar o numero de campeonatos ganhos
     *
     * @return numero de campeonatos ganhos
     */

    public int getCampg() {
        return campg;
    }

}

