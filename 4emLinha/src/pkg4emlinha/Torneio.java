/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg4emlinha;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Pc
 */
public class Torneio {

    Jogo jogo = new Jogo();
    quary quary = new quary();
    Scanner scanner = new Scanner(System.in);
    private String[][] idjogadores = new String[100][3];
    int participantes = 0;

    /**
     * cria os jogos.
     *
     * @throws IOException
     */
    public void criarTorneio() throws IOException {
        lerIds();
        int cach;
        for (int i = 0; i < participantes; i++) {
            for (int j = i + 1; j < participantes; j++) {
                jogo.setId1(Integer.parseInt(idjogadores[i][0]));
                jogo.setId2(Integer.parseInt(idjogadores[j][0]));
                jogo.setVitorias(false);
                cach = jogo.fazJogo(-1);
                if (cach == Integer.parseInt(idjogadores[i][0])) {
                    idjogadores[i][2] = Integer.toString(3 + Integer.parseInt(idjogadores[i][2].trim()));
                } else if (cach == Integer.parseInt(idjogadores[j][0])) {
                    idjogadores[j][2] = Integer.toString(3 + Integer.parseInt(idjogadores[j][2].trim()));
                } else {
                    idjogadores[i][2] = Integer.toString(3 + Integer.parseInt(idjogadores[i][2].trim()));
                    idjogadores[j][2] = Integer.toString(3 + Integer.parseInt(idjogadores[j][2].trim()));
                }
            }
        }
        checkPontos();
    }

    public void checkPontos() throws IOException {
        for (int i = 0; i < idjogadores.length; i++) {
            for (int j = i + 1; j < idjogadores.length; j++) {
                if (idjogadores[i][2] != null && idjogadores[j][2] != null) {
                    if (Integer.parseInt(idjogadores[i][2]) < Integer.parseInt(idjogadores[j][2])) {
                        String temp[] = idjogadores[i];
                        idjogadores[i] = idjogadores[j];
                        idjogadores[j] = temp;
                    }
                }
            }
        }
        for (int i = 0; i < idjogadores.length; i++) {
            if (idjogadores[i][2] != null ) {
                quary.updateTorneiosGanhos(Integer.parseInt(idjogadores[i][0]),Integer.parseInt(idjogadores[i][2]));
                System.out.println(i + 1 + "º - " + idjogadores[i][1] + " com " + idjogadores[i][2] + " pontos");
            }
        }
    }

    /**
     * Incere os jogadores no torneio
     *
     * @throws IOException
     */
    public void lerIds() throws IOException {
        System.out.println("Insira o numero de participantes");
        participantes = scanner.nextInt();
        if (participantes >= 3) {
            for (int i = 0; i < participantes; i++) {
                System.out.println("indique o seu id: ");
                idjogadores[i][0] = scanner.next();
                quary.getJogador(Integer.parseInt(idjogadores[i][0]));
                idjogadores[i][1] = quary.getNome();
                idjogadores[i][2] = "0";
            }
        } else {
            System.out.println("Variavel não reconhecida");
            System.exit(0);
        }
    }
}
