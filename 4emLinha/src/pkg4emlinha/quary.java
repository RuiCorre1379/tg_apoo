/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg4emlinha;

import java.io.*; //foi colocado o asterisco visto que estou a importar varias classes

/**
 * pesquisa as informações do jogador
 *
 * @author RuiCo
 */
public class quary extends Pessoa {

    final String File = "db4eml.txt";
    final String FileID = "db4emlID.txt";
    final String JogoID = "db4emlJogoID.txt";
    final String TorneioID = "db4emlTorneioID.txt";
    final String FileJogo = "db4emlJ.txt";
    final String FileTorneio = "db4emlT.txt";
    Pessoa b = new Pessoa();

    /**
     * le o ficheiro de base de dados
     *
     * @param file
     * @return ficeiro de base de dados
     * @throws FileNotFoundException
     * @throws IOException
     */
    public String getFile(String file) throws FileNotFoundException, IOException {
        String outFile = "";
        FileReader fr = new FileReader(file);
        char[] a = new char[3000];
        fr.read(a);   // reads the content to the array
        for (char c : a) {
            outFile += c;
        }
        return outFile;

    }

    /**
     * Verifica o id
     *
     * @param file
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void checkID(String file) throws FileNotFoundException, IOException {
        String cach = "";
        FileReader fr = new FileReader(file);
        char[] a = new char[3000];
        fr.read(a);   // reads the content to the array 
        for (char c : a) {
            cach += c;
        }
        this.njogador = Integer.parseInt(cach.trim());
    }

    /**
     * Cria um novo jogador no fichiro
     *
     * @return se criou jogador
     * @throws IOException
     */
    public int createJogador() throws IOException {
        checkID(FileID);
        String cach = this.njogador + "-" + this.nome + "-" + this.idade + "-0-0" + System.getProperty("line.separator");
        BufferedWriter f = null;
        try {
            f = new BufferedWriter(new FileWriter(File, true));
        } catch (IOException e) {
            return -1;
        }
        f.write(cach);
        f.close();
        //write to id
        FileWriter fid = null;
        try {
            fid = new FileWriter(FileID, false);
        } catch (IOException e) {
            return -1;
        }
        this.njogador++;
        fid.write(Integer.toString(this.njogador));
        fid.close();
        return this.njogador - 1;
    }

    /**
     * verifica se existe ficheiro de base de dados
     *
     * @throws IOException
     */
    public void checkFile() throws IOException {
        File f = new File(File);
        if (!f.isFile()) {
            PrintWriter writer = new PrintWriter(File, "UTF-8");
        }
        File fid = new File(FileID);
        if (!fid.isFile()) {
            PrintWriter writer = new PrintWriter(FileID);
            FileWriter fidWriter = new FileWriter(FileID, true);
            fidWriter.write("0");
            fidWriter.close();
        }
        File fj = new File(FileJogo);
        if (!fj.isFile()) {
            PrintWriter writer = new PrintWriter(FileJogo, "UTF-8");
        }
        File jid = new File(JogoID);
        if (!jid.isFile()) {
            PrintWriter writer = new PrintWriter(JogoID);
            FileWriter fidWriter = new FileWriter(JogoID, true);
            fidWriter.write("0");
            fidWriter.close();
        }
        File ft = new File(FileTorneio);
        if (!ft.isFile()) {
            PrintWriter writer = new PrintWriter(FileTorneio, "UTF-8");
        }
        File tid = new File(TorneioID);
        if (!tid.isFile()) {
            PrintWriter writer = new PrintWriter(TorneioID);
            FileWriter fidWriter = new FileWriter(TorneioID, true);
            fidWriter.write("0");
            fidWriter.close();
        }
    }

    /**
     * escreve nas vareaveis da pessoa os dados do jogador a que o id pretence
     *
     * @param id
     * @throws IOException
     */
    public void getJogador(int id) throws IOException {
        int i = 0;
        String file = getFile(File);
        String[] lines = file.split(System.getProperty("line.separator"));
        String[] jogador;
        do {
            jogador = lines[i].split("-");
            i++;
        } while (Integer.parseInt(jogador[0]) != id);
        this.nome = jogador[1];
        this.njogador = Integer.parseInt(jogador[0]);
        this.idade = Integer.parseInt(jogador[2]);
        this.jogosg = Integer.parseInt(jogador[3]);
        this.campg = Integer.parseInt(jogador[4]);
    }

    /**
     * incrementa o numero do jogos ganhos do id
     *
     * @param id
     * @throws IOException
     */
    public void updateJogosGanhos(int id) throws IOException {
        String file = getFile(File);
        String cach = "";
        String[] lines = file.split(System.getProperty("line.separator"));
        String[] cach1;
        String[][] matriz = new String[lines.length][5];
        for (int j = 0; j < lines.length; j++) {
            cach1 = lines[j].split("-");
            for (int i = 0; i < cach1.length; i++) {
                matriz[j][i] = cach1[i];
            }
            if (matriz[j][1] != null) {
                if (id == Integer.parseInt(matriz[j][0])) {
                    matriz[j][3] = Integer.toString(1 + Integer.parseInt(matriz[j][3].trim()));
                }
            }
        }
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < 5; j++) {
                if (matriz[i][1] != null) {
                    if (j == 4) {
                        cach += matriz[i][j];
                    } else {
                        cach += matriz[i][j] + "-";
                    }
                }
            }
            cach += System.getProperty("line.separator");
        }
        cach = cach.replaceAll("(?m)^[ \t]*\r?\n", "");
        BufferedWriter f = null;
        try {
            f = new BufferedWriter(new FileWriter(File, false));
        } catch (IOException e) {
        }
        f.write(cach);
        f.close();
    }

    /**
     * incrementa o numero do torneios ganhos do id
     *
     * @param id
     * @throws IOException
     */
    public void updateTorneiosGanhos(int id, int pontos) throws IOException {
        String file = getFile(File);
        String cach = "";
        String[] lines = file.split(System.getProperty("line.separator"));
        String[] cach1;
        String[][] matriz = new String[lines.length][5];
        for (int j = 0; j < lines.length; j++) {
            cach1 = lines[j].split("-");
            for (int i = 0; i < cach1.length; i++) {
                matriz[j][i] = cach1[i];
            }
            if (matriz[j][1] != null) {
                if (id == Integer.parseInt(matriz[j][0])) {
                    matriz[j][4] = Integer.toString(pontos + Integer.parseInt(matriz[j][4].trim()));
                }
            }
        }
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < 5; j++) {
                if (matriz[i][1] != null) {
                    if (j == 4) {
                        cach += matriz[i][j];
                    } else {
                        cach += matriz[i][j] + "-";
                    }
                }
            }
            cach += System.getProperty("line.separator");
        }
        cach = cach.replaceAll("(?m)^[ \t]*\r?\n", "");
        BufferedWriter f = null;
        try {
            f = new BufferedWriter(new FileWriter(File, false));
        } catch (IOException e) {
        }
        f.write(cach);
        f.close();
    }

    /**
     * output dos 10 melhores jogadores por torneios
     *
     * @throws IOException
     */
    public void sortFile() throws IOException {
        String file = getFile(File);
        String cach = "";
        String[] lines = file.split(System.getProperty("line.separator"));
        String[] cach1;
        String[][] matriz = new String[lines.length][5];
        for (int j = 0; j < lines.length; j++) {
            cach1 = lines[j].split("-");
            for (int i = 0; i < cach1.length; i++) {
                matriz[j][i] = cach1[i];
            }
        }
        for (int i = 0; i < matriz.length - 1; i++) {
            for (int j = i + 1; j < matriz.length - 1; j++) {
                if (Integer.parseInt(matriz[i][4]) < Integer.parseInt(matriz[j][4])) {
                    String temp[] = matriz[i];
                    matriz[i] = matriz[j];
                    matriz[j] = temp;
                }
            }
        }
        System.out.println();
        System.out.println("Os 10 melhores jogadores:");
        for (int i = 0; i < matriz.length && i < 10; i++) {
            System.out.println(i + 1 + "º - " + matriz[i][1] + " com " + matriz[i][4] + " pontos");
        }

    }

    /**
     * salva o jogo
     * @param cach
     * @throws IOException
     */
    public int saveGame(String cach) throws IOException {
        checkID(JogoID);
        BufferedWriter f = null;
        try {
            f = new BufferedWriter(new FileWriter(FileJogo, true));
        } catch (IOException e) {
        }
        f.write(this.njogador + "-" + cach+ "\n" );
        f.close();
        //write to id
        FileWriter fid = null;
        try {
            fid = new FileWriter(JogoID, false);
        } catch (IOException e) {
        }
        this.njogador++;
        fid.write(Integer.toString(this.njogador));
        fid.close();
        return this.njogador - 1;
    }

    /**
     * vai buscar o jogo
     * @param id
     * @return
     * @throws IOException 
     */
    public String getGame(int id) throws IOException {
        int i = 0;
        String file = getFile(FileJogo);
        String[] lines = file.split(System.getProperty("line.separator"));
        String[] jogador;
        do {
            jogador = lines[i].split("-");
            i++;
        } while (Integer.parseInt(jogador[0]) != id);
        return lines[i - 1];
    }
}
