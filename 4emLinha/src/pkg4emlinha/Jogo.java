/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg4emlinha;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Pc
 */
public class Jogo {

    int id1, id2;
    int totaljogadas = 0;
    boolean vitorias = true;
    quary quary = new quary();
    String[][] f = new String[7][15];

    public void setId1(int id1) {
        this.id1 = id1;
    }

    public void setId2(int id2) {
        this.id2 = id2;
    }

    public void setVitorias(boolean vitorias) {
        this.vitorias = vitorias;
    }

    /**
     * cria a estrutura do tabuleiro com as dimensoes 7 por 15,a razão de 15 é
     * para o uso de | e -
     *
     * @return
     */
    public void criarEstrutura() {

        for (int i = 0; i < f.length; i++) {

            for (int j = 0; j < f[i].length; j++) {
                if (j % 2 == 0) {
                    f[i][j] = "|";
                } else {

                    f[i][j] = " ";
                }

                if (i == 6) {
                    f[i][j] = "-";
                }
            }

        }
    }

    /**
     * mostra a estrutura do tabuleiro
     *
     * @param f
     * @throws java.io.IOException
     */
    public void mostrarEstrututa() throws IOException {
        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f[i].length; j++) {
                System.out.print(f[i][j]);
            }
            System.out.println();
        }
        quary.getJogador(id1);
        System.out.print("ID do jogador 1: " + quary.getNome());
        quary.getJogador(id2);
        System.out.println(" vs " + "ID do jogador 2: " + quary.getNome());
    }

    /**
     * coloca peça vermelha na coluna de 0 a 6 na menor linha disponivel faz o
     * out of bounds check
     *
     * @param f
     */
    public void pecaVermelha(String[][] f) throws IOException {
        int c;
        quary.getJogador(id1);
        System.out.print("Escolha uma coluna para colocar uma peça vermelha (0a6 mas seu quiser sair e guardar o jogo insira(9): " + quary.getNome());
        do {
            Scanner scan = new Scanner(System.in);
            c = 2 * scan.nextInt() + 1;
            if (c == 19) {
                System.out.println("jogo parado e guardado ");
                if (vitorias == false) {
                    String cach = "";
                    for (int i = 0; i < f.length - 1; i++) {
                        for (int j = 0; j < f[i].length; j++) {
                            if (j % 2 == 0) {
                                f[i][j] = "|";
                            } else {
                                cach += "-" + f[i][j];
                            }
                        }
                    }
                } else {
                    String cach = id1 + "-" + id2;
                    for (int i = 0; i < f.length - 1; i++) {
                        for (int j = 0; j < f[i].length; j++) {
                            if (j % 2 == 0) {
                                f[i][j] = "|";
                            } else {
                                cach += "-" + f[i][j];
                            }
                        }
                    }
                    System.out.println("O id do seu jogo é: " + quary.saveGame(cach));
                    System.exit(0);
                }
            }
        } while (c > 13 || c < 1);
        for (int i = 5; i >= 0; i--) {
            if (f[i][c] == " ") {
                f[i][c] = "R";
                break;
            }
        }
    }

    /**
     * coloca peça preta na coluna de 0 a 6 na menor linha disponivel faz o out
     * of bounds check
     *
     * @param f
     */
    public void pecaPreta(String[][] f) throws IOException {
        int c;
        quary.getJogador(id2);
        System.out.print("Escolha a coluna para colocar uma peça preta (0a6) mas se quiser sair e guardar o jogo insira(9): " + quary.getNome());
        do {
            Scanner scan = new Scanner(System.in);
            c = 2 * scan.nextInt() + 1;
            if (c == 19) {
                System.out.println("jogo parado e guardado ");
                if (vitorias == false) {
                    String cach = "";
                    for (int i = 0; i < f.length - 1; i++) {
                        for (int j = 0; j < f[i].length; j++) {
                            if (j % 2 == 0) {
                                f[i][j] = "|";
                            } else {
                                cach += "-" + f[i][j];
                            }
                        }
                    }
                } else {
                    String cach = id1 + "-" + id2;
                    for (int i = 0; i < f.length - 1; i++) {
                        for (int j = 0; j < f[i].length; j++) {
                            if (j % 2 == 0) {
                                f[i][j] = "|";
                            } else {
                                cach += "-" + f[i][j];
                            }
                        }
                    }
                    System.out.println("O id do seu jogo é: " + quary.saveGame(cach));
                    System.exit(0);
                }
            }
        } while (c > 13 || c < 1);
        for (int i = 5; i >= 0; i--) {
            if (f[i][c] == " ") {
                f[i][c] = "B";
                break;
            }
        }
    }

    /**
     * faz as verificaçoes na vertical,na horizontal e na diagonal
     *
     * @param f
     * @return
     */
    public String verVencedor(String[][] f) {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j += 2) {
                if ((f[i][j + 1] != " ")
                        && (f[i][j + 3] != " ")
                        && (f[i][j + 5] != " ")
                        && (f[i][j + 7] != " ")
                        && ((f[i][j + 1] == f[i][j + 3])
                        && (f[i][j + 3] == f[i][j + 5])
                        && (f[i][j + 5] == f[i][j + 7]))) {
                    return f[i][j + 1];
                }
            }
        }

        for (int i = 1; i < 15; i += 2) {
            for (int j = 0; j < 3; j++) {
                if ((f[j][i] != " ")
                        && (f[j + 1][i] != " ")
                        && (f[j + 2][i] != " ")
                        && (f[j + 3][i] != " ")
                        && ((f[j][i] == f[j + 1][i])
                        && (f[j + 1][i] == f[j + 2][i])
                        && (f[j + 2][i] == f[j + 3][i]))) {
                    return f[j][i];
                }
            }
        }

        for (int i = 0; i < 3; i++) {

            for (int j = 1; j < 9; j += 2) {
                if ((f[i][j] != " ")
                        && (f[i + 1][j + 2] != " ")
                        && (f[i + 2][j + 4] != " ")
                        && (f[i + 3][j + 6] != " ")
                        && ((f[i][j] == f[i + 1][j + 2])
                        && (f[i + 1][j + 2] == f[i + 2][j + 4])
                        && (f[i + 2][j + 4] == f[i + 3][j + 6]))) {
                    return f[i][j];
                }
            }
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 7; j < 15; j += 2) {
                if ((f[i][j] != " ")
                        && (f[i + 1][j - 2] != " ")
                        && (f[i + 2][j - 4] != " ")
                        && (f[i + 3][j - 6] != " ")
                        && ((f[i][j] == f[i + 1][j - 2])
                        && (f[i + 1][j - 2] == f[i + 2][j - 4])
                        && (f[i + 2][j - 4] == f[i + 3][j - 6]))) {
                    return f[i][j];
                }
            }
        }
        return null;
    }

    /**
     * faz o jogo e atraves da insereção de peças verifica o resultado do jogo
     */
    public int fazJogo(int idg) throws IOException {
        if (idg >= 0) {
            int v = 3;
            String[] cach = quary.getGame(idg).split("-");
            setId1(Integer.parseInt(cach[1]));
            setId2(Integer.parseInt(cach[2]));
            for (int i = 0; i < f.length; i++) {
                for (int j = 0; j < f[i].length; j++) {
                    if (j % 2 == 0) {
                        f[i][j] = "|";
                    } else {
                        f[i][j] = cach[v];
                        if (v<42) {
                            v++;
                        }
                    }

                    if (i == 6) {
                        f[i][j] = "-";
                    }

                }
            }
        } else {
            criarEstrutura();
        }
        boolean loop = true;
        int count = 0;
        int totaljogadas = 0;
        mostrarEstrututa();
        while (loop) {
            if (count % 2 == 0) {
                pecaVermelha(f);
            } else {
                pecaPreta(f);
            }
            count++;
            totaljogadas++;
            mostrarEstrututa();
            if (verVencedor(f) != null) {
                if (verVencedor(f) == "R") {
                    quary.getJogador(id1);
                    System.out.println(quary.getNome() + " ganhou.");
                    if (vitorias == true) {
                        quary.updateJogosGanhos(id1);
                    } else {
                        return id1;
                    }
                } else if (verVencedor(f) == "B") {
                    quary.getJogador(id2);
                    System.out.println(quary.getNome() + " ganhou.");
                    if (vitorias == true) {
                        quary.updateJogosGanhos(id2);
                    } else {
                        return id2;
                    }
                }
                loop = false;
            } else if (count == 42) {
                System.out.println("Empate");
                if (vitorias == false) {
                    return -1;
                }
            }
        }
        return 0;
    }

}
