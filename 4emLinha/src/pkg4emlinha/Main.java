/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg4emlinha;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author RuiCo
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        //invocar objeto de pesquisa
        quary quary = new quary();
        Torneio torneio = new Torneio();
        Jogo jogo = new Jogo();
        //verificar se o ficheiro de base de dados existe
        quary.checkFile();
        char selector;
        do {
            System.out.println("Insira:\n C para criar uma conta \n T para torneio \n J para jogo simples \n L para Lista de Top 10 \n S para sair");
            // ler o caracter incerido
            selector = scanner.next().charAt(0);
            //trasformar o caracter em maiuscula 
            selector = Character.toUpperCase(selector);
            switch (selector) {
                // criar conta 
                case 'C':
                    System.out.println("Introduza o seu nome");
                    quary.setNome(scanner.next());
                    System.out.println("Introduza a sua idade");
                    quary.setIdade(scanner.nextInt());
                    System.out.println("O seu id de jogado é: " + quary.createJogador());
                    selector = 'b';
                    break;
                //torneio
                case 'T':
                    torneio.criarTorneio();
                    break;
                //jogo simples
                case 'J':
                    System.out.println("insira o id do jogador 1: ");
                    jogo.setId1(scanner.nextInt());
                    System.out.println("insira o id do jogador 2: ");
                    jogo.setId2(scanner.nextInt());
                    jogo.fazJogo(-1);
                    break;
                //top10 em torneios
                case 'L':
                    quary.sortFile();
                    break;
                //retomar jogo
                case 'G':
                    System.out.println("Inira o Id do jogo que pretende contiuar");
                    jogo.fazJogo(scanner.nextInt());
                    break;
                //sair do programa
                case 'S':
                    System.exit(0);
                    break;
                //Caso contrario
                default:
                    selector = 'b';
                    break;

            }
        } while (selector == 'b');
    }
}
